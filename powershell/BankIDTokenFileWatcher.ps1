#
# BankIDTokenFileWatcher
#
# Starts BankID when a token is written to $TokenFile.
#
# Run this script on logon together with BankID. This script is intended to be
# used with an SSH server installed on the host so that a client can write this
# token to disk. You probably want to combine this script with the following:
# - https://gitlab.com/dabruh/dotfiles/-/blob/master/.local/share/applications/bankid.desktop
# - https://gitlab.com/dabruh/dotfiles/-/blob/master/.local/bin/send-bankid-token

$Token = $null
$LastToken = $null
$TokenFile = "C:\.bidtoken"

Write-Host "Starting file watcher for '$TokenFile'"

while ($true) {
    if (Test-Path $TokenFile) {
        $TokenFileData = Get-Item "$TokenFile"

        if (((Get-Date) - $TokenFileData.LastWriteTime).totalseconds -le 1){
            Write-Host "File modified: $TokenFile"
            $Token = Get-Content "$TokenFile"

            if ($Token -ne $LastToken) {
                Write-Host "New token: $Token"
                Start-Process "bankid:///?autostarttoken=$Token&redirect=null"
                Start-Sleep -Milliseconds 500
                $LastToken = $Token
            }
            else {
                Write-Host "Token unchanged: $Token"
            }
        }
    }
    Start-Sleep -Milliseconds 500
}
